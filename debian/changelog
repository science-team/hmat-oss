hmat-oss (1.10.0-1) unstable; urgency=medium

  * New upstream version 1.10.0
  * Removing upstreamed patch

 -- Pierre Gruet <pgt@debian.org>  Wed, 09 Oct 2024 09:33:10 +0200

hmat-oss (1.9.0-2) unstable; urgency=medium

  * Source-only upload to unstable
  * The package builds against g++14 (Closes: #1075068)

 -- Pierre Gruet <pgt@debian.org>  Wed, 10 Jul 2024 09:24:04 +0200

hmat-oss (1.9.0-1) experimental; urgency=medium

  * New upstream version 1.9.0
  * Removing upstreamed patch
  * Updating d/copyright
  * Bumping SONAME to libhmat-oss4
  * Raising Standards version to 4.7.0 (no change)
  * Build-depending on pkgconf instead of obsolete pkg-config
  * Patching the code to build against g++-14

 -- Pierre Gruet <pgt@debian.org>  Mon, 08 Jul 2024 09:53:45 +0200

hmat-oss (1.8.1-2) unstable; urgency=medium

  * Upload to unstable to start transition from libhmat-oss2 to libhmat-oss3

 -- Pierre Gruet <pgt@debian.org>  Sun, 13 Nov 2022 07:17:31 +0100

hmat-oss (1.8.1-1) experimental; urgency=medium

  * New upstream version 1.8.1
  * Refreshing patches
  * Bumping SOVERSION to 3, building libhmat-oss3 instead of libhmat-oss2
  * Passing HMAT_INSTALL_LIB_DIR to CMake instead of patching
  * Marking libhmat-oss-dev as Multi-Arch: same
  * Updating the autopkgtests after the addition of a new header in
    the examples directory
  * Depending on liblapacke-dev and pkg-config, the latter being needed to
    find lapacke

 -- Pierre Gruet <pgt@debian.org>  Thu, 10 Nov 2022 15:45:07 +0100

hmat-oss (1.7.1-3) unstable; urgency=medium

  * Solving issues that has the package FTBFS with g++-12:
    - incorrect precedence of + and ?: operators
    - unitialization error due to conditional initialization of an array
    (Closes: #1012945)
  * Raising Standards version to 4.6.1 (no change)
  * Marking the shared lib package as Multi-Arch: same

 -- Pierre Gruet <pgt@debian.org>  Sun, 19 Jun 2022 15:14:35 +0200

hmat-oss (1.7.1-2) unstable; urgency=medium

  * Upload to unstable to start the transition auto-hmat-oss
  * Making the autopkgtests depend on build-essential rather than gcc

 -- Pierre Gruet <pgt@debian.org>  Tue, 19 Apr 2022 15:08:33 +0200

hmat-oss (1.7.1-1) experimental; urgency=medium

  * New upstream version 1.7.1
  * Depending on debhelper-compat 13
  * Correcting d/watch, and upgrading it to version 4
  * Performing a SONAME bump, due to non backward-compatible ABI changes
  * Providing descriptions of packages with a bit more details
  * Adding myself as uploader
  * Simplifying debian/gbp.conf
  * Raising Standards version to 4.6.0:
    - Providing https Salsa links in Vcs-* fields
    - Extra priority is now optional
    - Using https form of format URL in d/copyright
    - Rules-Requires-Root: no
    - Dropping old-fashioned -dbg package
  * Removing all upstreamed patches
  * Removing build-dependency on quilt
  * Removing useless dependencies of the -dev package
  * Reworking d/rules:
    - Lighter override of dh_auto_configure
    - No tweaking of DEB_BUILD_OPTIONS
    - Hardening using /usr/share/dpkg/buildflags.mk
  * Refreshing d/copyright
  * Installing the library in /usr/lib/triplet
  * Correcting a spelling error in the source
  * Adding a Lintian override for not providing a symbols file
  * Adding an autopkgtest and providing it as example in the -dev package
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Pierre Gruet <pgt@debian.org>  Sat, 02 Apr 2022 20:44:55 +0200

hmat-oss (1.2.0-2.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Backport upstream fix for FTBFS with glibc 2.33. (Closes: #1002167)

 -- Adrian Bunk <bunk@debian.org>  Wed, 19 Jan 2022 18:14:34 +0200

hmat-oss (1.2.0-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS. (Closes: #897768)

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Sat, 02 May 2020 00:18:14 +0100

hmat-oss (1.2.0-2) unstable; urgency=medium

  * New patch: 0002-Fix-compilation-on-Linux-32-bits-systems.patch
    Fix compilation on 32bit systems, thanks Jérôme Robert.

 -- Denis Barbier <barbier@debian.org>  Thu, 20 Oct 2016 21:08:08 +0100

hmat-oss (1.2.0-1) unstable; urgency=low

  * New upstream version
  * This version builds with gcc 6.  (Closes: #831143)
  * New patch: 0001-make-build-reproducible.patch
    Makes builds reproducible.

 -- Denis Barbier <barbier@debian.org>  Wed, 19 Oct 2016 21:20:33 +0100

hmat-oss (1.0.4-2) unstable; urgency=low

  * Upload to unstable (Closes: #775738)

 -- Denis Barbier <barbier@debian.org>  Sun, 25 Jan 2015 11:22:12 +0000

hmat-oss (1.0.4-1) unstable; urgency=low

  * Initial release

 -- Julien Schueller <schueller@phimeca.com>  Sat, 22 Nov 2014 21:39:33 +0100
